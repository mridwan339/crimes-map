<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Crimes Map</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">



        <!-- Styles -->
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">

        <!-- Script -->
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
		<script src="js/appAngular.js"></script>
		
		
        
    </head>
    <body ng-app="theApp">
	<!-- BEGIN HEADER -->
    <header></header>
    <!-- END HEADER -->
	<!-- BEGIN LOAD PAGE -->
	<div ng-view></div>
	<!-- END LOAD PAGE -->
	
	<jquery-script></jquery-script>
	</body>
</html>

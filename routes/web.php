<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/api/v1/cities/', 'City@index');
Route::get('/api/v1/cities/detail/{id}', 'City@show');

Route::get('/api/v1/crimes/', 'Crimes@index');
Route::get('/api/v1/crimes/detail/{id}', 'Crimes@show');
Route::post('/api/v1/crimes/', 'Crimes@store');
Route::post('/api/v1/crimes/{id}', 'Crimes@update');
Route::get('/api/v1/crimes/destroy/{id}', 'Crimes@destroy');

Route::get('/api/v1/crimetypes/', 'Crimetypes@index');




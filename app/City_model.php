<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City_model extends Model
{
	protected $table = 'cm_cities';
	public $primaryKey  = 'cm_ci_id';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crimetypes_model extends Model
{
    protected $table = 'cm_crimestype';
	public $primaryKey  = 'cm_ct_id';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crimes_model extends Model
{
    protected $table = 'cm_crimes';
	public $primaryKey  = 'cm_cs_id';
}
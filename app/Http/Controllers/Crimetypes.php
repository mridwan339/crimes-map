<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use crimes_map\Http\Requests;
use App\Crimetypes_model;
class Crimetypes extends Controller
{
    public function index() {
		$crimetypes = Crimetypes_model::all();
		$crimetypesRows = count(Crimetypes_model::all());
		$arr=[];
		foreach($crimetypes as $crimetypes2){
		$arr2["cm_ct_id"]=$crimetypes2->cm_ct_id;
		$arr2["cm_ct_name"]=$crimetypes2->cm_ct_name;
		
		array_push($arr,$arr2);
		}
		$jsonStructure=array(
			"json_row"=>$crimetypesRows,
			"json_data"=>$arr
		);
		return $jsonStructure;
    }
}

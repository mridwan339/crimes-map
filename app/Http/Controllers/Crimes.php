<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use crimes_map\Http\Requests;

use App\Crimes_model;
use App\City_model;
use App\Crimetypes_model;
class Crimes extends Controller
{
    public function index() {
		$crimes=Crimes_model::all();
		$crimesRows = count(Crimes_model::all());
		$arr=[];
		foreach($crimes as $crimes2){
		
		$arr2["cm_cs_id"]=$crimes2->cm_cs_id;
		$arr2["cm_cs_title"]=$crimes2->cm_cs_title;
		$arr2["cm_cs_address"]=$crimes2->cm_cs_address;
		$arr2["cm_cs_city_id"]=$crimes2->cm_cs_city_id;
		$arr2["cm_cs_latitude"]=$crimes2->cm_cs_latitude;
		$arr2["cm_cs_longitude"]=$crimes2->cm_cs_longitude;
		$arr2["cm_cs_crimestype_id"]=$crimes2->cm_cs_crimestype_id;
		$arr2["cm_cs_description"]=$crimes2->cm_cs_description;
		$arr2["relationships"]=array(
			"cities"=>City_model::find($crimes2->cm_cs_city_id),
			"crimetypes"=>Crimetypes_model::find($crimes2->cm_cs_crimestype_id)
		);
		
		array_push($arr,$arr2);
		}
		$jsonStructure=array(
			"json_row"=>$crimesRows,
			"json_data"=>$arr
		);
		return $jsonStructure;
    }
	
    public function show($id) {
		$crimes=Crimes_model::find($id);
		$crimesRows = count(Crimes_model::find($id));
		$arr=[];
		
		$arr2["cm_cs_id"]=$crimes->cm_cs_id;
		$arr2["cm_cs_title"]=$crimes->cm_cs_title;
		$arr2["cm_cs_address"]=$crimes->cm_cs_address;
		$arr2["cm_cs_city_id"]=$crimes->cm_cs_city_id;
		$arr2["cm_cs_latitude"]=$crimes->cm_cs_latitude;
		$arr2["cm_cs_longitude"]=$crimes->cm_cs_longitude;
		$arr2["cm_cs_crimestype_id"]=$crimes->cm_cs_crimestype_id;
		$arr2["cm_cs_description"]=$crimes->cm_cs_description;
		$arr2["relationships"]=array(
			"cities"=>City_model::find($crimes->cm_cs_city_id),
			"crimetypes"=>Crimetypes_model::find($crimes->cm_cs_crimestype_id)
		);
		
		array_push($arr,$arr2);
		
		$jsonStructure=array(
			"json_row"=>$crimesRows,
			"json_data"=>$arr
		);
		return $jsonStructure;

    }
	public function store(Request $request) {
        $crimes = new Crimes_model;
        $params = json_decode(file_get_contents('php://input'),true);
		
		$crimes->cm_cs_title = $params['cm_cs_title'];
        $crimes->cm_cs_address = $params['cm_cs_address'];
        $crimes->cm_cs_city_id = $params['cm_cs_city_id'];
        $crimes->cm_cs_latitude = $params['cm_cs_latitude'];
		$crimes->cm_cs_longitude = $params['cm_cs_longitude'];
        $crimes->cm_cs_crimestype_id = $params['cm_cs_crimestype_id'];
        $crimes->cm_cs_description = $params['cm_cs_description'];
        $crimes->save();
		
		$jsonStructure=array(
			"json_row"=>1,
			"json_data"=>"ID ".$crimes->id." has been Created"
		);
		return $jsonStructure;
    }
    public function update(Request $request, $id) {
        $crimes = Crimes_model::find($id);
		$params = json_decode(file_get_contents('php://input'),true);
		
		$crimes->cm_cs_title = $params['cm_cs_title'];
        $crimes->cm_cs_address = $params['cm_cs_address'];
        $crimes->cm_cs_city_id = $params['cm_cs_city_id'];
        $crimes->cm_cs_latitude = $params['cm_cs_latitude'];
		$crimes->cm_cs_longitude = $params['cm_cs_longitude'];
        $crimes->cm_cs_crimestype_id = $params['cm_cs_crimestype_id'];
        $crimes->cm_cs_description = $params['cm_cs_description'];
        $crimes->save();
		
		$jsonStructure=array(
			"json_row"=>1,
			"json_data"=>"ID ".$id." has been Updated"
		);
		return $jsonStructure;
    }
    public function destroy(Request $request, $id) {
		$crimes = Crimes_model::find($id);

        $crimes->delete();

        $jsonStructure=array(
			"json_row"=>1,
			"json_data"=>"ID ".$id." has been Deleted"
		);
		return $jsonStructure;
    }
}

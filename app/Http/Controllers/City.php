<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use crimes_map\Http\Requests;
use App\City_model;

class City extends Controller
{
    public function index() {
		$cities=City_model::all();
		$citiesRows = count(City_model::all());
		$arr=[];
		foreach($cities as $cities2){
		$arr2["cm_ci_id"]=$cities2->cm_ci_id;
		$arr2["cm_ci_name"]=$cities2->cm_ci_name;
		$arr2["cm_ci_lat"]=$cities2->cm_ci_lat;
		$arr2["cm_ci_lon"]=$cities2->cm_ci_lon;
		
		array_push($arr,$arr2);
		}
		$jsonStructure=array(
			"json_row"=>$citiesRows,
			"json_data"=>$arr
		);
		return $jsonStructure;
    }
	
    public function show($id) {
		$cities=City_model::find($id);
		$citiesRows = count(City_model::find($id));
		$arr=[];
		$arr2["cm_ci_id"]=$cities->cm_ci_id;
		$arr2["cm_ci_name"]=$cities->cm_ci_name;
		$arr2["cm_ci_lat"]=$cities->cm_ci_lat;
		$arr2["cm_ci_lon"]=$cities->cm_ci_lon;
		
		array_push($arr,$arr2);
		
		$jsonStructure=array(
			"json_row"=>$citiesRows,
			"json_data"=>$arr
		);
		return $jsonStructure;
    }
}

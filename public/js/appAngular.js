var app = angular.module("theApp", ["ngRoute"]).constant('API_URL', 'http://localhost/crimes_map_laravel/public/api/v1/');
var map;
var marker;
var marker2;
var markers = [];
var id;
var actionBtn;

function loadSlideFilter() {
    $(document).ready(function() {
        var a = 0;
        $('.sign').on('click', function() {
            if (a == 0) {
                $('.filters').attr('style', 'top:64px');
                a = 1;
            } else {
                $('.filters').attr('style', 'top:-202px');
                a = 0;
            }
        });
    });
}

function loadMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -6.3876732, lng: 106.7477564 },
        zoom: 15
    });


    marker2 = new google.maps.Marker({
        position: { lat: -6.3876732, lng: 106.7477564 },
        map: map,
        draggable: true
    });

    marker2.setIcon('img/getPosition.png');
    google.maps.event.addListener(marker2, "dragend", function(event) {
        var latlon = String(this.position).replace("(", "").replace(")", "").split(",");
        if (actionBtn == "edit") {
            $("#locListBtn").click();
            //alert(id);
            setTimeout(function() {
                $("#locList [data-id='" + id + "']").click();
                setTimeout(function() {
                    marker2.setPosition(new google.maps.LatLng(latlon[0], latlon[1]));
                }, 10);
            }, 1000)

        } else {
            $("#locAddBtn").click();
        }
        $.getJSON("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlon[0] + "," + latlon[1] + "&sensor=true", function(result) {
            $.each(result, function(i, field) {
                var getLoc = (field[0].formatted_address).toString();
                setTimeout(function() {
                    $("#address").val(getLoc).focus();
                    setTimeout(function() {
                        $("#latitude").val(latlon[0]).focus();
                        setTimeout(function() {
                            $("#longitude").val(latlon[1]).focus();
                        }, 100);
                    }, 100);
                }, 100);
            });
        });

    });



}

function addMarker(lat, lon, idCrimetypes, titles, contents, filters, filters_array) {
    if (filters == "filter_yes") {
        for (i = 0; i < markers.length; i++) {
            if (markers[i].category == filters_array) {
                markers[i].setVisible(false);
            }
        }

    } else if (filters == "filter_display") {
        for (i = 0; i < markers.length; i++) {
            if (markers[i].category == filters_array) {
                markers[i].setVisible(true);
            }
        }
    } else {
        lat = parseFloat(lat);
        lon = parseFloat(lon);
        //var infowindow ;
        //var myOptions;
        if (lat !== 0, lon !== 0) {
            marker = new google.maps.Marker({
                position: { lat: lat, lng: lon },
                map: map,
                category: idCrimetypes,
                title: lat + "," + lon
            });



            if (idCrimetypes == 1) {
                marker.setIcon('img/robber.png');
            } else if (idCrimetypes == 2) {
                marker.setIcon('img/burglary.png');
            } else if (idCrimetypes == 3) {
                marker.setIcon('img/theft.png');
            } else if (idCrimetypes == 4) {
                marker.setIcon('img/shoot.png');
            } else if (idCrimetypes == 5) {
                marker.setIcon('img/vandalism.png');
            } else if (idCrimetypes == 6) {
                marker.setIcon('img/assault.png');
            } else if (idCrimetypes == 7) {
                marker.setIcon('img/arson.png');
            } else if (idCrimetypes == 8) {
                marker.setIcon('img/handcuffs.png');
            } else if (idCrimetypes == 9) {}

            /*myOptions = {
            	content: "<div class='i-box'><b>"+titles+"</b><br><div class='i-str'>"+contents+"</div></div>",
            	disableAutoPan: false,
            	maxWidth: 0,
            	alignBottom: true,
            	pixelOffset: new google.maps.Size(-16, -11),
            	zIndex: null,
            	closeBoxURL: "",
            	pane: "floatPane",
            	enableEventPropagation: false,
            	infoBoxClearance: "10px",
            	position: marker.position
            };
            infowindow = new InfoBox(myOptions);*/
            var infowindow = new google.maps.InfoWindow({
                content: "<div class='i-box'><b>" + titles + "</b><br><div class='i-str'>" + contents + "</div></div>"
            });
            google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    infowindow.open(map, marker);
                    setTimeout(function() {
                        infowindow.close();
                    }, 10000)
                }
            })(marker));
            markers.push(marker);
            //markers[0].setVisible(false);
            console.log(markers);
        }
    }
}

function focusToMap(e) {
    lat = parseFloat($(e).attr('data-lat'));
    lng = parseFloat($(e).attr('data-lng'));
    id = $(e).attr('data-id');
    var position = new google.maps.LatLng(lat, lng);

    marker2.setPosition(new google.maps.LatLng(lat, lng));
    /*marker = new google.maps.Marker({
        position: { lat: lat, lng: lng },
        map: map,
        draggable: true
    });
    marker.setIcon('img/getPosition.png');*/
    map.setCenter(position);
}

function loadModal() {
    var modal0 = document.getElementById('locList');
    var btn0 = document.getElementById("locListBtn");
    var span0 = document.getElementsByClassName("close")[0];
    btn0.onclick = function() {
        modal0.style.display = "block";
    }
    span0.onclick = function() {
        modal0.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal0) {
            modal0.style.display = "none";
        }
    }
    var modal = document.getElementById('locAdd');
    var btn = document.getElementById("locAddBtn");
    var span = document.getElementsByClassName("close")[1];
    btn.onclick = function() {
        modal.style.display = "block";
    }
    span.onclick = function() {
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function filterCategory(e) {
    var as = $(e).attr('data-crimeTypesId');
    if (e.checked) {
        addMarker(0, 0, 0, 0, 0, "filter_display", as);
    } else {
        addMarker(0, 0, 0, 0, 0, "filter_yes", as);
    }

}
app.controller("homeController", function($window, $scope, $http, API_URL) {
    $scope.$on('$viewContentLoaded', function() {
        //alert('ok');
    });
    $scope.$on('loadJScript', function(event, args) {
        //alert('ok');
    });
});

app.controller("crimepController", function($window, $scope, $http, API_URL) {
    $scope.toast = function(message) {
        var x = document.getElementById("snackbar")
        x.className = "show";
        x.innerHTML = message;
        setTimeout(function() {
            x.className = x.className.replace("show", "");
            location.reload();
        }, 3000);
    }
    $scope.populate = function(type, key) {
        if (type == "select,checkbox") {
            if (key == "crimetypes") {
                $http({
                    method: 'GET',
                    url: API_URL + key,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    $scope.crimetypes = response.json_data;
                });
            } else if (key == "cities") {
                $http({
                    method: 'GET',
                    url: API_URL + key,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    $scope.cities = response.json_data;
                });
            }
        }

        if (type == "table") {
            if (key == "crimes") {
                $http({
                    method: 'GET',
                    url: API_URL + key,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    $scope.crimes = response.json_data;
                });
            }
        }

        if (type == "map") {
            if (key == "crimes") {
                $http({
                    method: 'GET',
                    url: API_URL + key,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    $scope.crimesLoadmap = response.json_data;
                    angular.forEach($scope.crimesLoadmap, function(item) {
                        console.log(item.cm_cs_latitude + " / " + item.cm_cs_longitude);
                        addMarker(item.cm_cs_latitude, item.cm_cs_longitude, item.cm_cs_crimestype_id, item.cm_cs_title, item.cm_cs_address, "filter_no", 0);
                    })

                });
            }
        }

    }
    $scope.titleForm = "Add Location";

    $scope.config_form = function(typeform) {
        //alert(typeform);
        if (typeform == "add") {
            actionBtn = "add";
            $scope.titleForm = "Add Location";
            $scope.getID = "";
            $scope.contentForm = { "cm_cs_title": "The Title", "cm_cs_address": "Where the Address ?", "cm_cs_city_id": "", "cm_cs_latitude": "-6.318923", "cm_cs_longitude": "106.810907", "cm_cs_crimestype_id": "", "cm_cs_description": "Tell the Description" };
            setTimeout(function() {
                $("#title").focus();
                setTimeout(function() {
                    $("#description").focus();
                    setTimeout(function() {
                        $("#address").focus();
                        setTimeout(function() {
                            $("#latitude").focus();
                            setTimeout(function() {
                                $("#longitude").focus();
                            }, 10);
                        }, 10);
                    }, 10);
                }, 10);
            }, 10);
        } else if (typeform == "edit") {
            $scope.titleForm = "Edit Location";
            actionBtn = "edit";
            $scope.getID = id;
            $("#locList").attr("style", "display:none;");
            $("#locAdd").attr("style", "display:block;");
            $http({
                method: 'GET',
                url: API_URL + 'crimes/detail/' + id,
                headers: { 'Content-Type': 'multipart/form-data' }
            }).success(function(response) {
                $scope.contentForm = response.json_data[0];
                setTimeout(function() {
                    $("#title").focus();
                    setTimeout(function() {
                        $("#description").focus();
                        setTimeout(function() {
                            $("#address").focus();
                            setTimeout(function() {
                                $("#latitude").focus();
                                setTimeout(function() {
                                    $("#longitude").focus();
                                }, 10);
                            }, 10);
                        }, 10);
                    }, 10);
                }, 10);

            });
        }
    }
    $scope.save = function() {
        //alert(id);
        var url = API_URL + "crimes";
        if (id > 0) {
            url += "/" + id;
        }

        $http({
                url: url,
                method: "POST",
                data: { "cm_cs_title": $scope.contentForm.cm_cs_title, "cm_cs_address": $scope.contentForm.cm_cs_address, "cm_cs_city_id": $scope.contentForm.cm_cs_city_id, "cm_cs_latitude": $scope.contentForm.cm_cs_latitude, "cm_cs_longitude": $scope.contentForm.cm_cs_longitude, "cm_cs_crimestype_id": $scope.contentForm.cm_cs_crimestype_id, "cm_cs_description": $scope.contentForm.cm_cs_description }
            })
            .then(function(response) {
                //alert($scope.contentForm.cm_cs_title+" Saved");
                //location.reload();
                $scope.toast($scope.contentForm.cm_cs_title + " Saved");

            });


    }
    $scope.destroy = function(e, a) {
        //alert($scope.getID);
        var desId = e;
        var desName = a;
        //alert(desId);
        var url = API_URL + "crimes/destroy/" + desId;

        $http({
                url: url,
                method: "GET",
            })
            .then(function(response) {
                //alert(desName+" Destroyed");
                //location.reload();
                $scope.toast(desName + " Destroyed");
            });


    }
    $scope.$on('$viewContentLoaded', function() {
        loadMap();
        $('document').ready(function() {
            $('#select-city').on('change', function() {
                var getLat = $(this).find(':selected').attr('data-lat');
                var getLon = $(this).find(':selected').attr('data-lon');
                //alert(getLat+' /'+getLon);
                //loadMap();
                var idcity = $scope.contentForm.cm_cs_city_id;
                $http({
                    method: 'GET',
                    url: API_URL + "cities/detail/" + idcity,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    map.setCenter(new google.maps.LatLng(response.json_data[0].cm_ci_lat, response.json_data[0].cm_ci_lon));
                });
            });
        });
        loadModal();
        loadSlideFilter();
        $scope.populate('select,checkbox', 'crimetypes');
        $scope.populate('select,checkbox', 'cities');
        $scope.populate('table', 'crimes');
        $scope.populate('map', 'crimes');
    });
    $scope.$on('loadJScript', function(event, args) {
        loadMap();
        $('document').ready(function() {
            $('#select-city').on('change', function() {
                //alert(getLat+' /'+getLon);
                //loadMap(getLat,getLon,0,0,0);
                var idcity = $scope.contentForm.cm_cs_city_id;
                $http({
                    method: 'GET',
                    url: API_URL + "cities/detail/" + idcity,
                    //data: $.param($scope.employee),
                    headers: { 'Content-Type': 'multipart/form-data' }
                }).success(function(response) {
                    map.setCenter(new google.maps.LatLng(response.json_data[0].cm_ci_lat, response.json_data[0].cm_ci_lon));
                });
            });
        });
        loadModal();
        loadSlideFilter();
        $scope.populate('select,checkbox', 'crimetypes');
        $scope.populate('select,checkbox', 'cities');
        $scope.populate('table', 'crimes');
        $scope.populate('map', 'crimes');
        /*setTimeout(function(){
        	addMarker(0,0,0,0,0,"filter_yes",2);
        },10000);*/
    });

});

app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "../resources/views/sections/homepage.html",
            controller: 'homeController'
        })
        .when("/open-map", {
            templateUrl: "../resources/views/sections/crimes-map.html",
            controller: 'crimepController'
        })
        .otherwise({
            redirectTo: "/"
        });
});
app.directive('jqueryScript', function() {
    return {
        restrict: 'E',
        link: function(rootScope, scope, element, attrs) {



            $(document).ready(function() {
                $.getScript("https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js", function(data, textStatus, jqxhr) {
                    console.log("Materialize [OK]");
                    $.getScript("http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyA67PpHpK4gXlQPVMNjJ5b14OOyuXoydx4", function(data, textStatus, jqxhr) {
                        //$.getScript("http://demo-ee.com/assets/js/google_maps/infobox.js.pagespeed.jm.35gYCRGeJH.js", function(data, textStatus, jqxhr) {
                        console.log("Map [OK]");
                        rootScope.$broadcast('loadJScript');
                        console.clear();
                        //});
                    });
                });
            });




        },
    };
});

app.directive('header', function() {
    return {
        restrict: 'E',
        templateUrl: '../resources/views/sections/header.html'
    };
});
-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Nov 2016 pada 14.04
-- Versi Server: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crimes_map`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cm_cities`
--

CREATE TABLE IF NOT EXISTS `cm_cities` (
  `cm_ci_id` int(10) unsigned NOT NULL,
  `cm_ci_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cm_ci_lat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cm_ci_lon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `cm_cities`
--

INSERT INTO `cm_cities` (`cm_ci_id`, `cm_ci_name`, `cm_ci_lat`, `cm_ci_lon`, `created_at`, `updated_at`) VALUES
(1, 'DKI Jakarta', '-6.2293867', '106.6894293', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(2, 'Bogor', '-6.5950182', '106.7218508', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(3, 'Depok', '-6.3876732', '106.7477564', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(4, 'Tanggerang', '-6.1765128', '106.5799929', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(5, 'Bekasi', '-6.2841797', '106.8332882', '2016-11-07 17:00:00', '2016-11-07 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cm_crimes`
--

CREATE TABLE IF NOT EXISTS `cm_crimes` (
  `cm_cs_id` int(10) unsigned NOT NULL,
  `cm_cs_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cm_cs_address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cm_cs_city_id` int(10) unsigned NOT NULL,
  `cm_cs_latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cm_cs_longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cm_cs_crimestype_id` int(10) unsigned NOT NULL,
  `cm_cs_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `cm_crimes`
--

INSERT INTO `cm_crimes` (`cm_cs_id`, `cm_cs_title`, `cm_cs_address`, `cm_cs_city_id`, `cm_cs_latitude`, `cm_cs_longitude`, `cm_cs_crimestype_id`, `cm_cs_description`, `created_at`, `updated_at`) VALUES
(1, 'Crime 1', 'Jl.Moh Kahfi 1', 1, '-6.318923', '106.810907', 3, 'Crime 1 in Jakarta', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(6, 'The Titles', 'Where the Address ?', 3, '-6.318923', '106.810907', 1, 'Tell the Descriptions', '2016-11-15 04:50:39', '2016-11-15 05:00:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cm_crimestype`
--

CREATE TABLE IF NOT EXISTS `cm_crimestype` (
  `cm_ct_id` int(10) unsigned NOT NULL,
  `cm_ct_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `cm_crimestype`
--

INSERT INTO `cm_crimestype` (`cm_ct_id`, `cm_ct_name`, `created_at`, `updated_at`) VALUES
(1, 'robber', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(2, 'burglary', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(3, 'theft', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(4, 'shoot', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(5, 'vandalism', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(6, 'assault', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(7, 'arson', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(8, 'handcuffs', '2016-11-07 17:00:00', '2016-11-07 17:00:00'),
(9, 'other', '2016-11-07 17:00:00', '2016-11-07 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_11_08_002421_cm_cities', 1),
('2016_11_08_002446_cm_crimestype', 1),
('2016_11_08_002450_cm_crimes', 1),
('2016_11_08_011105_cm_vw_crimes', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cm_cities`
--
ALTER TABLE `cm_cities`
  ADD PRIMARY KEY (`cm_ci_id`);

--
-- Indexes for table `cm_crimes`
--
ALTER TABLE `cm_crimes`
  ADD PRIMARY KEY (`cm_cs_id`),
  ADD KEY `cm_crimes_cm_cs_city_id_foreign` (`cm_cs_city_id`),
  ADD KEY `cm_crimes_cm_cs_crimestype_id_foreign` (`cm_cs_crimestype_id`);

--
-- Indexes for table `cm_crimestype`
--
ALTER TABLE `cm_crimestype`
  ADD PRIMARY KEY (`cm_ct_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cm_cities`
--
ALTER TABLE `cm_cities`
  MODIFY `cm_ci_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cm_crimes`
--
ALTER TABLE `cm_crimes`
  MODIFY `cm_cs_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cm_crimestype`
--
ALTER TABLE `cm_crimestype`
  MODIFY `cm_ct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `cm_crimes`
--
ALTER TABLE `cm_crimes`
  ADD CONSTRAINT `cm_crimes_cm_cs_city_id_foreign` FOREIGN KEY (`cm_cs_city_id`) REFERENCES `cm_cities` (`cm_ci_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cm_crimes_cm_cs_crimestype_id_foreign` FOREIGN KEY (`cm_cs_crimestype_id`) REFERENCES `cm_crimestype` (`cm_ct_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmCrimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cm_crimes', function (Blueprint $table) {
            $table->increments('cm_cs_id');
			$table->string('cm_cs_title', 255);
			$table->longText('cm_cs_address');
			$table->integer('cm_cs_city_id')->length(10)->unsigned();
			$table->string('cm_cs_latitude', 255);
			$table->string('cm_cs_longitude', 255);
			$table->integer('cm_cs_crimestype_id')->length(10)->unsigned();
			$table->longText('cm_cs_description');
            $table->timestamps();
        });
		Schema::table('cm_crimes', function (Blueprint $table) {
			$table->foreign('cm_cs_city_id')
			->references('cm_ci_id')->on('cm_cities')
			->onDelete('cascade');
			$table->foreign('cm_cs_crimestype_id')
			->references('cm_ct_id')->on('cm_crimestype')
			->onDelete('cascade');	
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cm_crimes');
    }
}

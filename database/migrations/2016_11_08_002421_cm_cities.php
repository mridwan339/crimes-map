<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cm_cities', function (Blueprint $table) {
            $table->increments('cm_ci_id');
			$table->string('cm_ci_name', 100);
			$table->string('cm_ci_lat', 100);
			$table->string('cm_ci_lon', 100);
            $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cm_cities');
    }
}
